'''
@file fibonacci.py
@brief This fibonacci.py file is Lab 1 for ME 305, and it returns a Fibonacci number
       for an inputted index. 
@details The program should be able to handle positive and
         negative numbers, decimals, and non-numbers accordingly.
'''

print("Welcome to Cole's Fibonnaci Enumerator.")
print("     Type 'exit' to leave program.     ")

def fib(idx):
    '''
    @brief     This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired 
               Fibonacci number.
             

    Parameters
    ----------
    idx : Integer
        This parameter is the desired index at which you want a Fibonacci number.

    Returns
    -------
    Number
        This is the desired Fibonnaci number at the inputted index.

    '''
    z = [0,1]
    if idx == 0:
        return 0
    elif idx == 1:
        return 1
    elif idx == -1:
        return -1
    else:
        for i in range(abs(idx)):
            z.append(z[0] + z[1])
            z.pop(0)
        if idx > 0:
            return z[-2]*(1)
        else:
            return z[-2]*(-1)

if __name__ == '__main__':
    
    while True:
        try:
            idx = input ('Fibonacci Index Number: ')
            if idx == 'exit' or idx == 'EXIT' or idx == 'Exit':
                print('Thanks for exiting!')
                break
            else:
                idx = float(idx)
                if not idx % 1:
                    print('Fibonacci number at '
                          'index {:} is {:}.'.format((int(idx)),fib(int(idx))))
                else:
                    print('Decimals are not accepted.')
        except KeyboardInterrupt:
            print('')
            print('Thanks for exiting!')
            break
        except:
            print("Only integers are accepted.")    